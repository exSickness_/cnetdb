#define _GNU_SOURCE
#define _XOPEN_SOURCE 700

#include <dirent.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

/*
 * REFERENCES
 * http://stackoverflow.com/questions/4204666/how-to-list-files-in-a-directory-in-a-c-program
 *
 */


int recurseDir(char* curDir, char* target);


int main(int argc, char* argv[])
{
	if (argc !=3) {
		fprintf(stderr, "Usage: %s <word> <directory>\n", argv[1]);
		return(-1);
	}

	if(recurseDir(argv[2], argv[1]) == 1) {
		printf("Found string in sub-directories!\n");
	}
	else {
		printf("No string in sub-directories!\n");
	}

	return(0);
}

int recurseDir(char* curDir, char* target)
{
	DIR *d;
	struct dirent *dir;
	d = opendir(curDir);
	chdir(curDir);
	char* dirBuf = malloc(sizeof(*dirBuf) * 256);
	printf("current directory: %s\n", getcwd(dirBuf, 256));
	free(dirBuf);

	if (d) {
		while ((dir = readdir(d)) != NULL) {

			if(strncmp("..", dir->d_name, 3) == 0) {
				continue;
			}
			if(strncmp(".", dir->d_name, 2) == 0) {
				continue;
			}
			if (dir->d_type == DT_REG) {
				printf("%s\n", dir->d_name);
				size_t sz = 256;

				FILE* fp = fopen(dir->d_name, "r");
				if(!fp) {
					printf("Cannot open file.\n");
					continue;
				}

				char* buf = malloc(sizeof(*buf) * sz );
				if(!buf) {
					printf("Cannot allocate space for buffer.\n");
					free(fp);
					continue;
				}
				memset(buf, '\0', sz);

				while(getline(&buf, &sz, fp) != -1)
				{
					if(strstr(buf, target))
					{
						printf("\tFOUND TARGET!\n");
						free(buf);
						fclose(fp);
						closedir(d);
						return(1);
					}
					// printf("%s", buf);
				}
				free(buf);
				fclose(fp);
			}

			if (dir->d_type == DT_DIR) {
				printf("GOING IN >>%s\n", dir->d_name);
				if( recurseDir(dir->d_name, target) == 1) {
					closedir(d);
					return(1);
				}
				printf("<< GOING OUT\n");
			}
		}

		closedir(d);
	}

	chdir("..");
	return(0);
}