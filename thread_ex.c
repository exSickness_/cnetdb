#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

static int koala_death_count;


void* kill_em_all(void* amount)
{
	for(int n = 0; n < *(int*)amount; n++)
	{
		koala_death_count+=1;
	}
	return(NULL);
}

int main(int argc, char const *argv[])
{
	if(argc < 2)
	{
		fprintf(stderr, "Usage: %s\n", argv[0]);
		return(2);
	}

	int to_kill = strtol(argv[1], NULL, 10);

	pthread_t one, two;

	pthread_create(&one, NULL, kill_em_all, (void*)&to_kill);
	printf("created thread id:%zu\n", one);
	pthread_join(one, NULL);

	printf("%d\n", koala_death_count);
	/* code */
	return 0;
}