#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

void echo_handler(int sig)
{
	printf("I caught the %d!\n", sig);
}

int main(void)
{
	struct sigaction nerdlinger;
	memset (&nerdlinger, '\0', sizeof(nerdlinger));
	nerdlinger.sa_handler = echo_handler;

	if(sigaction(SIGHUP, &nerdlinger, NULL) == -1) {
		perror ("Error: cannot handle SIGHUP");
		return(-1);
	}

	for(;;) {
		sleep(5);
	}

	return(0);
}