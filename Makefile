
CFLAGS+=-std=c11
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline

LDFLAGS+=-pthread

sigEcho: sigEcho.o
thread_ex: thread_ex.o
rgrep: rgrep.o

.PHONY: clean debug

clean:
	-rm sigEcho *.o

debug: CFLAGS+=-g
debug: sigEcho
