#define _XOPEN_SOURCE 600

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

int main(void)
{
	// pid_t pid = getpid();

	int fd[2];
	if(pipe(fd) < 0)
	{
		perror("Could not set up comms.\n");
		return(2);
	}

	pid_t child = fork();


	if(child == 0) // parent
	{
		char buf[256] = {0};
		int* w_fd = &fd[1];
		while(!feof(stdin))
		{
			fgets(buf, sizeof(buf), stdin);
			write(*w_fd, buf, strlen(buf));
		}
		return(0);
	}
	else if(child > 0) // child
	{
		char buf[256] = {0};
		FILE* input = fdopen(fd[0], "r");
		FILE* output = fopen("output", "w");;
		if(!input) {
			fprintf(stderr, "%s\n", "Could not open file from pipe.");
		}
		while(!feof(input))
		{
			if(fgets(buf, sizeof(buf), input)) {
				fprintf(output, "%s", buf);
			}
		}
		fclose(output);
		fclose(input);
	}
	else
	{
		perror("Could not forkify.\n");
	}

	close(fd[0]);
	close(fd[1]);

	return(0);
}