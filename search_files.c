#include <stdio.h>
#include <string.h>
#include <dirents.h>

#include <sys/types.h>

char* needle;

void* search_file(void* fname)
{
	if(!fname) {
		return(NULL);
	}

	char* filename = (char*) fname;
	FILE* fp = fopen(filename, "r");
	if(!fp) {
		perror("Could not open file");
		return(NULL);
	}

	char buf[128];

	while(fgets(buf, sizeof(buf), fp))
	{
		if(strstr(buf, needle))
		{
			fclose(fp);
			return(filename);
		}
		size_t len = strlen(buf);
		if(buf[len-1] != '\n') {
			fseek(fp, -(int)strlen(needle), SEEK_CUR);
		}
	}

	if(!feof(fp)) {
		fprintf(stderr, "Failure in reading %s\n", filename);
	}

	fclose(fp);

	return(NULL);
}

int main(int argc, char *argv[])
{
	if(argc != 2) {
		fprintf(stderr, "Usage: %s <needle>\n", argv[0]);
		return(1);
	}

	needle = argv[1];

	void* result = search_file((void*)"/etc/passwd");

	if(result) {
		printf("Found!\n");
	}
	else
	{
		printf("Not found :( ){: &; :} ;: !\n");
	}

	return(0);
}

